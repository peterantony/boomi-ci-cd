#!/bin/bash
# This result of this script is to deploy a Packaged Component to a nominated environment
# It does this by doing three things:
# 1. extracts the source Environment GUID
# 2. extracts the target Environment GUID
# 3. deploys the Packaged Component to the Target Environment

while getopts ":p:a:t:n:" options; do
        case "${options}" in
                p) _packageId=${OPTARG} ;;
                a) _acct=${OPTARG} ;;
                t) _targetEnv=${OPTARG} ;;
                n) _deployNotes=${OPTARG} ;;
                *) ;;
        esac
done

if [ -z "$_packageId" -o -z "$_acct" -o -z "$_targetEnv" -o -z "$_deployNotes" ]
then
        echo Usage $(basename $0) -p packageId -a AccountID -t TargetEnv -n Notes
        exit 1
fi

_WD=/home/boomi
cd $_WD
mkdir $_WD/tmp 2> /dev/null

_token="{EnterBoomiToken}"
_multiEnv=$(echo $_targetEnv | grep '~' | wc -l)
if [ "$_multiEnv" -ne 0 ]
then
        while true
        do
                _envIdx=$((_envIdx + 1))
                #_envName=$(echo $_targetEnv | awk -F '~' '{ print $($(echo $_envIdx)) }')
                _envName=$(echo $_targetEnv | cut -f"$_envIdx" -d"~")
                if [ -z "$_envName" ]
                then
                        break
                else
                        _envArr+=("$_envName")
                fi
        done
else
        _envArr+=("$_targetEnv")
fi

for envName in "${_envArr[@]}"
do
        echo Promoting to $envName...

        ###get Environment
        cp $_WD/getEnvironment.req.tmpl $_WD/getEnvironment.req
        sed -i "s/{1}/$envName/" $_WD/getEnvironment.req
        URL="https://api.boomi.com/api/rest/v1/$_acct/Environment/query/"
        curl -s -u "$_token" -X POST -H "Content-Type: application/xml" -d @$_WD/getEnvironment.req -o $_WD/tmp/out.xml.proc $URL
        if [ -f $_WD/tmp/out.xml.proc ]
        then
                xmllint -format $_WD/tmp/out.xml.proc > $_WD/tmp/out.xml
                _envId=$(xmllint --xpath "//*/@id" $_WD/tmp/out.xml | sed 's/"//g' | awk -F '=' '{ print $2 }')
        fi

        ###Deploy
        cp $_WD/deployPackagedComponent.req.tmpl $_WD/deployPackagedComponent.req
        #_packageId="abbc1b05-f288-453b-be61-4b420f931af2"
        sed -i "s/{1}/$_envId/" $_WD/deployPackagedComponent.req
        sed -i "s/{2}/$_packageId/" $_WD/deployPackagedComponent.req
        sed -i "s/{3}/$_deployNotes/" $_WD/deployPackagedComponent.req

        URL="https://api.boomi.com/api/rest/v1/$_acct/DeployedPackage/"
        curl -s -u "$_token" -X POST -H "Content-Type: application/xml" -d @$_WD/deployPackagedComponent.req -o $_WD/tmp/out.xml.proc $URL
done
