#!/bin/bash
# This result of this script is to deploy a Packaged Component to a nominated environment
# It does this by doing three things:
# 1. creates a Packaged Component
# 2. extracts the target Environment GUID
# 3. deploys the Packaged Component to the Environment

while getopts "o:b:v:n:p:t:a:" options; do
        case "${options}" in
                o) _objName=${OPTARG} ;;
                b) _acct=${OPTARG} ;;
                v) _deployVer=${OPTARG} ;;
                n) _deployNotes=${OPTARG} ;;
                p) _targetEnv=${OPTARG} ;;
                t) _boomiDCUrl=${OPTARG} ;;
                a) _taskId=${OPTARG} ;;
                *) ;;
        esac
done

echo Task ID is "$_taskId"
echo BDC "$_boomiDCUrl"
if [ -z "$_objName" -o -z "$_acct" -o -z "$_deployVer" -o -z "$_deployNotes" ]
then
        echo Usage: $(basename $0) -o objectName -b AccountID -v ComponentVersion -n Notes -p TargetEnv -a AsanaTaskID -t DevOpsURL
        exit 1
fi

_WD=/home/boomi
cd $_WD
mkdir $_WD/tmp 2> /dev/null
/bin/rm -f $_WD/tmp/*

URL="https://api.boomi.com/api/rest/v1/$_acct/ComponentMetadata/query"
echo Processing "'$_objName'"...

cp $_WD/compMetadata.req.tmpl $_WD/compMetadata.req
sed -i "s/{1}/$_objName/" $_WD/compMetadata.req

_token="{EnterBoomiToken}"
curl -s -u "$_token" -X POST -H "Content-Type: application/xml" -d @$_WD/compMetadata.req -o $_WD/tmp/out.xml.proc $URL
if [ -f $_WD/tmp/out.xml.proc ]
then
        xmllint -format $_WD/tmp/out.xml.proc > $_WD/tmp/out.xml
        _compId=$(xmllint --xpath "//*/@componentId" $_WD/tmp/out.xml | sed 's/"//g' | awk -F '=' '{ print $2 }')
fi

if [ "$_targetEnv" == "prod" ]
then
        cp $_WD/promotePackagedComponent.req.tmpl $_WD/promotePackagedComponent.req
        sed -i "s/{1}/$_compId/" $_WD/promotePackagedComponent.req
        sed -i "s/{2}/$_deployVer/" $_WD/promotePackagedComponent.req
        URL="https://api.boomi.com/api/rest/v1/$_acct/PackagedComponent/query"
        curl -s -u "$_token" -X POST -H "Content-Type: application/xml" -d @$_WD/promotePackagedComponent.req -o $_WD/tmp/out.xml.proc $URL
        if [ -f $_WD/tmp/out.xml.proc ]
        then
                xmllint -format $_WD/tmp/out.xml.proc > $_WD/tmp/out.xml
                _packageId=$(xmllint --xpath "//*/*[local-name()='packageId']" $_WD/tmp/out.xml | awk -F '<bns:packageId>' '{ print $2}'|cut -c1-36)
                echo $_packageId > $_WD/tmp/pkgId.proc
                echo $_objName > $_WD/tmp/pkgName.proc
                _notes="GitLab: Deployed to Prod."
        fi
else
        ###Create Packaged Component
        cp $_WD/crPackagedComponent.req.tmpl $_WD/crPackagedComponent.req
        sed -i "s/{1}/$_compId/" $_WD/crPackagedComponent.req
        _ver=$_deployVer
        sed -i "s/{2}/$_ver/" $_WD/crPackagedComponent.req
        _notes="$_deployNotes"
        sed -i "s/{3}/$_notes/" $_WD/crPackagedComponent.req

        URL="https://api.boomi.com/api/rest/v1/$_acct/PackagedComponent/"
        curl -s -u "$_token" -X POST -H "Content-Type: application/xml" -d @$_WD/crPackagedComponent.req -o $_WD/tmp/out.xml.proc $URL
        if [ -f $_WD/tmp/out.xml.proc ]
        then
                xmllint -format $_WD/tmp/out.xml.proc > $_WD/tmp/out.xml
                _packageId=$(xmllint --xpath "//*/*[local-name()='packageId']" $_WD/tmp/out.xml | awk -F '<bns:packageId>' '{ print $2}'|cut -c1-36)
                echo $_compId > $_WD/tmp/compId.proc
                echo $_packageId > $_WD/tmp/pkgId.proc
                echo $_objName > $_WD/tmp/pkgName.proc
        fi
fi

if [ ! -z "$_targetEnv" ]
then
        /home/boomi/promotePackagedComponent.sh -p "$_packageId" -a $_acct -t "$_targetEnv" -n "$_notes"
        if [ "$_targetEnv" != "prod" ]
        then
                      /home/boomi/execTestSuite.sh -b $_boomiDCUrl -p "$_compId" -t 9
        elif [ "$_targetEnv" == "prod" ]
	then
		echo Updating Asana task...
       		cp $_WD/updAsanaTask.req.tmpl $_WD/updAsanaTask.req
		_dt=$(date +"%d-%m-%Y %H:%M")
        	sed -i "s/{2}/$_dt/" $_WD/updAsanaTask.req
        	URL="https://app.asana.com/api/1.0/tasks/$_taskId"
                _token="{EnterAsanaToken}"
                curl -s -X GET -H "Authorization: Bearer $_token" --write-out '%{http_code}' -o $_WD/tmp/out.json.proc $URL
                if [ -f $_WD/tmp/out.json.proc ]
                then
                        _taskNotes=$(jq -r .data.notes "$_WD/tmp/out.json.proc")
                        echo notes: $_taskNotes
                        _taskNotes=$(echo $_taskNotes | sed 's/\//\\\//g')
                        echo notes: $_taskNotes
                        sed -i "s/{1}/$_taskNotes/" $_WD/updAsanaTask.req
                        cat $_WD/updAsanaTask.req
                fi
                
                _exitCode=$(curl -s -X PUT -H "Authorization: Bearer $_token" -H 'Content-Type: application/json' --write-out '%{http_code}' -d @$_WD/updAsanaTask.req -o $_WD/tmp/out.json.proc $URL)
                echo exit code: $_exitCode
                if [ "$_exitCode" -ne 200 ]
                then
                        echo An error occurred updating Task!
                else
                        echo Task updated OK
                fi
	fi
fi
